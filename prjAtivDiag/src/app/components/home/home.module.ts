import { HomeComponent } from './home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopoComponent } from './topo/topo.component';
import { PromocaoComponent } from './promocao/promocao.component';
import { AlugarComponent } from './alugar/alugar.component';
import { ComprarComponent } from './comprar/comprar.component';



@NgModule({
  declarations: [
    TopoComponent,
    HomeComponent,
    PromocaoComponent,
    AlugarComponent,
    ComprarComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
